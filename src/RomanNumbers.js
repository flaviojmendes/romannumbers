function RomanNumbers() {
	
}

RomanNumbers.prototype.convert = function(number) {
	var numbArray = [	[1000, "M"],[900, "CM"],[100, "C"],
						[90, "XC"],	[50, "L"],	[40, "XL"],
						[10, "X"],	[9, "IX"],	[5, "V"],
						[4, "IV"],	[1, "I"]];
	var romanNumber = "";
	
	for(var i=0 ; i < numbArray.length ; i++) {

		while(number >= numbArray[i][0]) {
			romanNumber += numbArray[i][1];
			number -= numbArray[i][0];
		}
	}

	return romanNumber;  
};