describe("RomanNumbers", function() {
  var romanNumbers;
  
  beforeEach(function() {
    romanNumbers = new RomanNumbers();
  });

  it("should convert 1 to 3", function() {
    expect(romanNumbers.convert(1)).toBe("I");
    expect(romanNumbers.convert(2)).toBe("II");
    expect(romanNumbers.convert(3)).toBe("III");
  });

  it("should convert 4", function() {
    expect(romanNumbers.convert(4)).toBe("IV");
  });

  it("should convert 5", function() {
    expect(romanNumbers.convert(5)).toBe("V");
  });

  it("should convert 9", function() {
    expect(romanNumbers.convert(9)).toBe("IX");
  });

  it("should convert 10, 20, 30", function() {
    expect(romanNumbers.convert(10)).toBe("X");
    expect(romanNumbers.convert(20)).toBe("XX");
    expect(romanNumbers.convert(30)).toBe("XXX");
  });


  it("should convert 40", function() {
    expect(romanNumbers.convert(40)).toBe("XL");
  });

  
  it("should convert 50", function() {
    expect(romanNumbers.convert(50)).toBe("L");
  });  


  it("should convert 90", function() {
    expect(romanNumbers.convert(90)).toBe("XC");
  });  


  it("should convert 100", function() {
    expect(romanNumbers.convert(100)).toBe("C");
  }); 

  it("should convert 153", function() {
    expect(romanNumbers.convert(153)).toBe("CLIII");
  });  

  it("should convert 1000", function() {
    expect(romanNumbers.convert(1000)).toBe("M");
  });  

  it("should convert 1999", function() {
    expect(romanNumbers.convert(1999)).toBe("MCMXCIX");
  });  
});
